#! usr/bin/python
# #coding=utf-8
import re
import random
from pathos.multiprocessing import ProcessingPool as Pool
import multiprocessing
import logging
import os

random.seed(0)
ratio = 0.8
dir = '../data/selected-%.1f/' % ratio
adir = 'selected-%.1f' % ratio
dir = '../data/'
username_dict = {}
if os.path.exists(dir + 'private_username_dict.csv'):
    for line in open(dir + 'private_username_dict.csv'):
        s = re.match("(.*),(\\d*)", line).groups()
        username_dict[int(s[1])] = s[0]

weibo_dict = {}
if os.path.exists(dir + 'private_weibo_dict.csv'):
    for line in open(dir + 'private_weibo_dict.csv'):
        s = re.match("\"(.*)\",(\\d*),\\d*", line).groups()
        weibo_dict[int(s[1])] = s[0]

reverseAlignment = {}
alignment = {}
align_train = {}
for line in open(dir + 'alignment.csv'):
    s = line.split(',')
    alignment[int(s[0])] = int(s[1])
    if random.random() < 0.8:
        align_train[int(s[0])] = int(s[1])
    reverseAlignment[int(s[1])] = int(s[0])
douban_users = alignment.keys()
weibo_users = alignment.values()

douban_name = {}
for line in open(dir + 'douban_username.csv'):
    s = re.match("(\\d*),(.*)", line).groups()
    douban_name[int(s[0])] = map(int, s[1].split(';'))
weibo_name = {}
for line in open(dir + 'weibo_username.csv'):
    s = re.match("(\\d*),(.*)", line).groups()
    if len(s[1]) > 0:
        weibo_name[int(s[0])] = map(int, s[1].split(';'))
    else:
        weibo_name[int(s[0])] = []
logging.info('Basic Data (%s) Loaded' % dir)


def fetch_name(ids):
    return ''.join(map(lambda x: username_dict[x], ids))


def fetch_weibo(ids):
    return ''.join(map(lambda x: weibo_dict[x], ids))


douban_network = None
weibo_network = None
douban_action = {}
weibo_action = {}
douban_items = set()

params = {}


def set_param(name, var):
    params[name] = var


def load_networks():
    global douban_network
    global weibo_network
    if douban_network != None:
        return
    douban_network = {}
    weibo_network = {}
    for douban_id in alignment:
        douban_network[douban_id] = set()
        weibo_network[alignment[douban_id]] = set()
    for line in open(dir + 'douban_network.csv'):
        s = map(int, line.split(','))
        if (s[0] != s[1]):
            douban_network[s[0]].add(s[1])
            douban_network[s[1]].add(s[0])
    for line in open(dir + 'weibo_network.csv'):
        s = map(int, line.split(','))
        if (s[0] != s[1]):
            weibo_network[s[0]].add(s[1])
            weibo_network[s[1]].add(s[0])
    logging.info('Network Data Loaded')


def load_douban_action_raw():
    global douban_action
    douban_action['raw'] = dict(map(lambda x: (x, set()), alignment))
    for line in open(dir + 'douban_movielog.csv'):
        s = map(int, line.split(','))
        douban_action['raw'][s[0]].add(s[1])
        douban_items.add(s[1])
    logging.info('Douban Action (Raw) Loaded')


def load_douban_action():
    global douban_action
    for cate in ['train', 'test']:
        douban_action[cate] = dict(map(lambda x: (x, set()), alignment))
        for line in open(dir + 'douban_movielog.%s.csv' % cate):
            s = map(int, line.split(','))
            douban_action[cate][s[0]].add(s[1])
            douban_items.add(s[1])
    logging.info('Douban Action Loaded')


def split_douban_action(ratio):
    global douban_action
    douban_action['train'] = dict(map(lambda x: (x, []), alignment))
    douban_action['test'] = dict(map(lambda x: (x, []), alignment))
    for uid in douban_action['raw']:
        for iid in douban_action['raw'][uid]:
            if random.random() < ratio:
                douban_action['train'][uid].append(iid)
            else:
                douban_action['test'][uid].append(iid)
    for cate in ['train', 'test']:
        with open(dir + 'douban_movielog.%s.csv' % cate, 'w') as f:
            f.write('\n'.join(
                filter(lambda x: len(x) > 0, map(lambda x: '\n'.join(
                    map(lambda y: '%d,%d' % (x, y), douban_action[cate][x])), douban_action[cate]))))
    logging.info('Douban Action Split Finished')


def load_weibo_action_raw():
    global weibo_action
    weibo_action['raw'] = dict(map(lambda x: (x, []), alignment.values()))
    cnt = 0
    for line in open(dir + 'weibo_content.csv'):
        cnt += 1
        if cnt % 1000000 == 0:
            print '\r%d Million Weibo Loaded' % (cnt / 1000000)
        weibo = Weibo(line, keep=True)
        weibo_action['raw'][weibo.uid].append(weibo)
    logging.info('Weibo Action (Raw) Loaded')


def load_weibo_action():
    global weibo_action
    if 'train' in weibo_action:
        return
    limit = 0
    if 'weibo_action_limit' in params:
        limit = params['weibo_action_limit']
    for cate in ['train', 'test']:
        weibo_action[cate] = dict(map(lambda x: (x, []), alignment.values()))
        cnt = 0
        for line in open(dir + 'weibo_content.%s.csv' % cate):
            cnt += 1
            if cnt % 1000000 == 0:
                print 'Loading Weibo Action (%s) : %dM' % (cate, cnt / 1000000)
            if cnt == limit:
                break
            weibo = Weibo(line)
            if len(weibo.content) < 20:
                continue
            weibo_action[cate][weibo.uid].append(weibo)
    logging.info('Weibo Action Loaded')


def split_weibo_action(ratio):
    random.seed(0)
    global weibo_action
    weibo_action['train'] = dict(map(lambda x: (x, []), alignment.values()))
    weibo_action['test'] = dict(map(lambda x: (x, []), alignment.values()))
    for uid in weibo_action['raw']:
        for weibo in weibo_action['raw'][uid]:
            if random.random() < ratio:
                weibo_action['train'][uid].append(weibo)
            else:
                weibo_action['test'][uid].append(weibo)
    for cate in ['train', 'test']:
        with open(dir + 'weibo_content.%s.csv' % cate, 'w') as f:
            f.write('\n'.join(
                filter(lambda x: len(x) > 0, map(lambda x: '\n'.join(
                    map(lambda y: y.line, weibo_action[cate][x])), weibo_action[cate]))))
    logging.info('Weibo Action Split Finished')


class Weibo:

    def __init__(self, line, keep=False):
        s = line.split(',')
        if keep:
            self.line = line.strip()
        self.uid = int(s[0])
        # self.original = int(s[1])
        self.content = map(int, filter(lambda x: len(x) > 0, s[2].split(';')))
        # self.retweet = int(s[3])
        # self.pic = int(s[4])
        # self.like = int(s[5])
        # self.comment = int(s[6])
        # self.platform = int(s[7])


def userSelector():
    users = set(alignment.keys())
    print 'Current Count : %d' % len(users)
    users = set(filter(lambda x: fetch_name(
        douban_name[x]).find("已注销") == -1, users))
    print 'Remove [已注销] in Douban : %d' % len(users)
    load_douban_action_raw()

    minDoubanAction = 1
    users = set(filter(lambda x: len(
        douban_action['raw'][x]) >= minDoubanAction, users))
    print 'Remove <%d actions in Douban : %d' % (minDoubanAction, len(users))

    load_weibo_action_raw()
    minWeiboAction = 1
    users = filter(lambda x: len(weibo_action['raw'][
                   alignment[x]]) >= minWeiboAction, users)
    print 'Remove <%d actions in Weibo : %d' % (minWeiboAction, len(users))

    weibo_users = set(map(lambda x: alignment[x], users))
    load_networks()
    # minEdgeCount = 5
    # douban_degree = dict(map(lambda x: (x, 0), users))
    # weibo_degree = dict(map(lambda x: (x, 0), weibo_users))
    # for i in weibo_network:
    #     if i in weibo_users:
    #         for j in weibo_network[i]:
    #             if j in weibo_users:
    #                 weibo_degree[i] += 1
    # for i in douban_network:
    #     if i in users:
    #         for j in douban_network[i]:
    #             if j in users:
    #                 douban_degree[i] += 1
    # while True:
    #     changed = False
    #     for id in set(users):
    #         if douban_degree[id] < minEdgeCount or weibo_degree[alignment[id]] < minEdgeCount:
    #             changed = True
    #             users.remove(id)
    #             weibo_users.remove(alignment[id])
    #             for i in douban_network[id].intersection(users):
    #                 douban_degree[i] -= 1
    #             for i in weibo_network[alignment[id]].intersection(weibo_users):
    #                 weibo_degree[i] -= 1
    #     if not changed:
    #         break
    # print 'Remove <%d edges in One Network : %d' % (minEdgeCount, len(users))
    with open(dir + '%s/alignment.csv' % adir, 'w') as f:
        f.write('\n'.join(map(lambda x: '%d,%d' % (x, alignment[x]), users)))
    os.system('cp %sprivate* %s/' % (dir, dir, adir))
    os.system('cp %sdouban_username.csv %s/' % (dir, dir, adir))
    os.system('cp %sweibo_username.csv %s/' % (dir, dir, adir))
    with open(dir + '/douban_network.csv' % adir, 'w') as f:
        for i in users:
            for j in douban_network[i]:
                if j in users:
                    f.write('%s,%s\n' % (i, j))
    with open(dir + '%s/weibo_network.csv' % adir, 'w') as f:
        for i in weibo_users:
            for j in weibo_network[i]:
                if j in weibo_users:
                    f.write('%s,%s\n' % (i, j))
    with open(dir + '%s/douban_movielog.csv' % adir, 'w') as f:
        for uid in users:
            for iid in douban_action['raw'][uid]:
                f.write('%s,%s\n' % (uid, iid))
    with open(dir + '%s/weibo_content.csv' % adir, 'w') as f:
        for i in weibo_users:
            for weibo in weibo_action['raw'][i]:
                f.write(weibo.line + '\n')


def dataSplitor():
    load_douban_action_raw()
    split_douban_action(ratio)
    load_weibo_action_raw()
    split_weibo_action(ratio)
