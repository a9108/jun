import logging
import Data as d
import multiprocessing

import numpy as np
from sklearn import metrics

# from tfMF import MF as Model


def calcAUCTrain(param):
    uid, preds = param
    truth = np.zeros(len(d.douban_items))
    for iid in d.douban_action['train'][uid]:
        truth[iid] = 1
    fpr, tpr, thresholds = metrics.roc_curve(truth, preds, pos_label=1)
    return metrics.auc(fpr, tpr)


def calcAUCTest(param):
    uid, preds = param
    truth = np.zeros(len(d.douban_items))
    for iid in d.douban_action['test'][uid]:
        truth[iid] = 1
    ind = list(d.douban_items - d.douban_action['train'][uid])
    preds = map(lambda x: preds[x], ind)
    truth = map(lambda x: truth[x], ind)
    fpr, tpr, thresholds = metrics.roc_curve(truth, preds, pos_label=1)
    return metrics.auc(fpr, tpr)


def evaluate(model):
    logging.info('Running Evaluator for Movie Recommendation')
    pool = multiprocessing.Pool()

    testUsers = filter(lambda x: len(
        d.douban_action['test'][x]) > 0, d.alignment.keys())
    preds = map(lambda uid: model.predictDouban(
        uid, d.douban_items), testUsers)
    aucs = pool.map(calcAUCTest, zip(testUsers, preds))

    trainUsers = filter(
        lambda x: len(d.douban_action['train'][x]) > 0,
        d.alignment.keys())
    predsTrain = map(lambda uid: model.predictDouban(
        uid, d.douban_items), trainUsers)
    aucsTrain = pool.map(calcAUCTrain, zip(trainUsers, predsTrain))
    pool.close()
    logging.warn('Movie - AUC@Test : %.4f' % (sum(aucs) / len(aucs)))
    logging.warn('Movie - AUC@Train : %.4f' %
                 (sum(aucsTrain) / len(aucsTrain)))


def evaluateCold(model, thres):
    logging.info('Running Cold-Start Evaluator for Movie Recommendation')
    pool = multiprocessing.Pool()

    testUsers = filter(lambda x: len(
        d.douban_action['test'][x]) > 0, d.alignment.keys())
    preds = map(lambda uid: model.predictDouban(
        uid, d.douban_items), testUsers)
    aucs = pool.map(calcAUCTest, zip(testUsers, preds))
    size = map(lambda x: len(d.douban_action['train'][x]), testUsers)

    pool.close()

    auc_groups = []
    for t in thres:
        sum = 0
        cnt = 0
        for s, a in zip(size, aucs):
            if s <= t:
                cnt += 1
                sum += a
        auc_groups.append(sum / cnt)
    logging.warn('Movie - AUC@Test (Cold) : %s' %
                 (', '.join(map(lambda x: '%.4f' % x, auc_groups))))

# if __name__ == '__main__':
#     d.load_douban_action()

#     model = Model(round=10)
#     model.train(d)
#     print 'Training Finished'

#     evaluate(model)
