import sys
import logging

logging.basicConfig(format='[%(levelname)s] %(asctime)s: %(message)s', level=logging.INFO)

K = int(sys.argv[1])
logging.error("Global Setting - K = %d" % K)

import Data as d

# d.set_param('weibo_action_limit', 100)
# d.load_networks()
# # d.load_douban_action()
# d.load_weibo_action(1000000)

from JUMA import JUMA

import MovieRec
import TweetRec
import NetAlign

d.load_douban_action()

juma = JUMA(d, evaluator={'movie': MovieRec.evaluate, 'tweet': TweetRec.evaluate, 'aligner': NetAlign.evaluateAUC},
             douban_K=K, weibo_K=K, word_K=K,
             evaluationRound=10, dump_iteration=10, isolate=False, expname='8-%d' % K)
juma.train(d, updatingNets=['douban'], round=100)
juma.train(d, updatingNets=['weibo'], round=20)
juma = JUMA(d, evaluator={'movie': MovieRec.evaluate, 'tweet': TweetRec.evaluate, 'aligner': NetAlign.evaluateAUC},
            douban_K=K, weibo_K=K, word_K=K,
            evaluationRound=10, dump_iteration=10, isolate=False, expname='8-%d' % K, constant=True)

juma.train(d, updatingNets=['aligner'], round=500)
juma = JUMA(d, evaluator={'movie': MovieRec.evaluate, 'tweet': TweetRec.evaluate, 'aligner': NetAlign.evaluateAUC},
            douban_K=K, weibo_K=K, word_K=K,
            evaluationRound=10, dump_iteration=10, isolate=False, expname='8-%d' % K)
juma.setname(juma.expname + '-fine')
juma.train(d, updatingNets=['aligner', 'douban', 'weibo'], round=100)


thres = [0, 1, 2, 5, 10, 20, 30, 40, 50, 100000000]
MovieRec.evaluateCold(juma, thres)
TweetRec.evaluateCold(juma, thres)

# models = {}
# models['aligner'] = NetAlign.ExactMatch(d)
# models['aligner'] = NetAlign.TopAlign(pickle.load(open('../models/editDistances', 'rb')))
# models['aligner'] = juma
# models['movie'] = juma
# models['tweet'] = juma


# def evaluate():
#     if models['movie'] != None:
#         MovieRec.evaluate(models['movie'])
#     if models['aligner'] != None:
#         NetAlign.evaluateAUC(models['aligner'])
#     if models['tweet'] != None:
#         TweetRec.evaluate(models['tweet'])
#
#
# evaluate()
