import pickle
import os
import numpy as np
from sklearn import metrics
import random
import math
import multiprocessing
import Data as d
from functools import partial
import logging


def fetchWeibos(uid):
    curw = []
    for weibo in d.weibo_action['train'][uid]:
        curw.extend(weibo.content)
    curw = curw[:min(len(curw), 1000)]
    curw = filter(lambda x: x > 100, curw)
    return curw


def initZ(w, K):
    return list(np.random.randint(K, size=len(w)))


def updateZ(param, phi, K):
    theta, w = param

    def find(w):
        posi = map(lambda x: theta[x] * phi[x][w], range(K))
        v = random.random() * sum(posi)
        for i, t in enumerate(posi):
            if v <= t:
                return i
            v -= t
        return K - 1

    return map(find, w)


def calcPerp(param, phi, K):
    theta, ws = param
    res = map(lambda w: sum(map(lambda x: theta[x] * phi[x][w], range(K))), ws)
    res = map(math.log, filter(lambda x: x > 0, res))
    return (sum(res), len(res))


class TopicModel:
    def __init__(self, d, K=20, alpha=1, beta=1):
        self.K = K
        self.alpha = alpha
        self.beta = beta
        self.wordcount = max(d.weibo_dict) + 1
        self.userId = dict(zip(d.weibo_users, range(len(d.weibo_users))))
        self.dir = '../models/tm-%d' % self.K
        if os.path.exists(self.dir + '.phi'):
            self.theta = pickle.load(open(self.dir + '.theta', 'rb'))
            self.phi = pickle.load(open(self.dir + '.phi', 'rb'))
            self.w = pickle.load(open(self.dir + '.w', 'rb'))
            self.z = pickle.load(open(self.dir + '.z', 'rb'))
            print 'Model Loaded'
        else:
            self.theta = [[1. / self.K] * self.K for uid in self.userId]
            self.phi = [[1. / self.wordcount] * self.wordcount for k in range(self.K)]
            pool = multiprocessing.Pool()
            self.w = pool.map(fetchWeibos, d.weibo_users)
            print 'W Initialized'
            self.z = pool.map(partial(initZ, K=self.K), self.w)
            print 'Z Initialized'
            pool.close()

    def save(self):
        print 'Saving Topic Model'
        pickle.dump(self.theta, open(self.dir + '.theta', 'wb'))
        pickle.dump(self.phi, open(self.dir + '.phi', 'wb'))
        pickle.dump(self.w, open(self.dir + '.w', 'wb'))
        pickle.dump(self.z, open(self.dir + '.z', 'wb'))
        print 'Topic Model Saved'

    def train(self, d, r):
        for i in range(r):
            self.updatePhi()
            print 'Phi updated'
            self.updateTheta()
            print 'Theta updated'
            pool = multiprocessing.Pool()
            self.z = pool.map(partial(updateZ, phi=self.phi, K=self.K), zip(self.theta, self.w))
            pool.close()
            print 'Z updated'
            print 'Perplexity : %.2f' % self.calcPerp()
        self.showTopics()

    def showTopics(self):
        for k in range(self.K):
            order = range(self.wordcount)
            order.sort(key=lambda x: -self.phi[k][x])
            print 'Topic #%d : %s' % (k, ','.join(map(lambda x: d.weibo_dict[x], order[0:100])))

    def calcPerp(self):
        pool = multiprocessing.Pool()
        logs = pool.map(partial(calcPerp, phi=self.phi, K=self.K), zip(self.theta, self.w))
        pool.close()
        sum, cnt = reduce(lambda x, y: (x[0] + y[0], x[1] + y[1]), logs)
        return math.exp(-sum / cnt)

    def updatePhi(self):
        cnt = [[0] * self.wordcount for k in range(self.K)]
        for ws, zs in zip(self.w, self.z):
            for w, z in zip(ws, zs):
                cnt[z][w] += 1
        for k in range(self.K):
            tot = sum(cnt[k])
            self.phi[k] = map(lambda x: (self.beta + cnt[k][x]) / float(self.beta * self.K + tot),
                              range(self.wordcount))

    def updateTheta(self):
        for uid in range(len(self.theta)):
            zs = self.z[uid]
            cnt = [0] * self.K
            for z in zs:
                cnt[z] += 1
            tot = sum(cnt)
            self.theta[uid] = map(lambda x: (cnt[x] + self.alpha) / float(self.alpha * self.K + tot), range(self.K))

    def predict(self, users, docs):
        users = map(lambda uid: self.userId[uid], users)
        pool = multiprocessing.Pool()
        res = pool.map(partial(predictUserDocs, theta=self.theta, phi=self.phi, K=self.K), zip(users, docs))
        pool.close()
        return res


def predictUserDocs(param, theta, phi, K):
    uid, docs = param

    def calc(doc):
        if len(doc) == 0:
            return -1e100
        posi = map(lambda w: math.log(sum(map(lambda k: theta[uid][k] * phi[k][w], range(K)))), doc)
        return sum(posi) / len(posi)

    res = map(calc, docs)
    return res


def hashWeibo(weibo):
    return reduce(lambda x, y: x * 123 + y, weibo)


d.load_networks()
d.load_weibo_action()
weibos = [weibo.content for weibos in d.weibo_action['test'].values() for weibo in weibos]
weibos = dict(map(lambda x: (hashWeibo(x), x), weibos)).values()
logging.info('Total testing weibos : %d' % len(weibos))
testUsers = filter(lambda x: len(d.weibo_action['test'][x]) > 0, d.weibo_action['test'].keys())
trainUsers = filter(lambda x: len(d.weibo_action['train'][x]) > 0, d.weibo_action['train'].keys())
nneg = 1000


def fetchNegWeibos(uid, n=100):
    pos = map(lambda x: x.content, d.weibo_action['train'][uid] + d.weibo_action['test'][uid])
    hashes = set(map(hashWeibo, pos))
    for weibo in pos:
        hashes.add(hashWeibo(weibo))
    neg = []
    for i in range(n):
        weibo = weibos[random.randint(0, len(weibos) - 1)]
        hash = hashWeibo(weibo)
        if not hash in hashes:
            neg.append(weibo)
            hashes.add(hash)
    return neg


def genSamples(uid):
    pos = map(lambda x: x.content, d.weibo_action['test'][uid])
    hashes = set(map(hashWeibo, pos))
    for weibo in d.weibo_action['train'][uid]:
        hashes.add(hashWeibo(weibo.content))
    neg = []
    for fid in d.weibo_network[uid]:
        for weibo in d.weibo_action['test'][fid]:
            neg.append(weibo.content)
    if len(neg) == 0:
        for i in range(nneg):
            weibo = weibos[random.randint(0, len(weibos) - 1)]
            hash = hashWeibo(weibo)
            if not hash in hashes:
                neg.append(weibo)
                hashes.add(hash)
    random.shuffle(neg)
    neg = neg[:min(len(neg), nneg)]
    return pos + neg


def genSamplesTrain(uid):
    pos = map(lambda x: x.content, d.weibo_action['train'][uid])
    neg = map(lambda x: weibos[random.randint(0, len(weibos) - 1)], range(nneg))
    return pos + neg


samples = map(genSamples, testUsers)
truth = map(lambda uid, sample: np.concatenate
((np.ones(len(d.weibo_action['test'][uid])), np.zeros(len(sample) - len(d.weibo_action['test'][uid]))))
            , testUsers, samples)
logging.info('Weibo - Testing samples generated')

samplesTrain = map(genSamplesTrain, trainUsers)
truthTrain = map(lambda uid, sample: np.concatenate
((np.ones(len(d.weibo_action['train'][uid])), np.zeros(len(sample) - len(d.weibo_action['train'][uid]))))
                 , trainUsers, samplesTrain)
logging.info('Weibo - Training samples generated')


def evaluate(model):
    preds = model.predictWeibo(testUsers, samples)
    aucs = []
    MAP = []
    for i, (t, p) in enumerate(zip(truth, preds)):
        # p = map(lambda x: x + random.random() * 1e-15, p)
        ind = range(len(t))
        random.shuffle(ind)
        t = map(lambda x: t[x], ind)
        p = map(lambda x: p[x], ind)
        fpr, tpr, thresholds = metrics.roc_curve(t, np.array(p), pos_label=1)
        aucs.append(metrics.auc(fpr, tpr))
        MAP.append(metrics.average_precision_score(t, p))
    logging.warn('Tweet - AUC@Test : %.4f , MAP : %.4f' % (sum(aucs) / len(aucs), sum(MAP) / len(MAP)))

    predsTrain = model.predictWeibo(trainUsers, samplesTrain)
    aucsTrain = []
    MAPTrain = []
    for i, (t, p) in enumerate(zip(truthTrain, predsTrain)):
        fpr, tpr, thresholds = metrics.roc_curve(t, np.array(p), pos_label=1)
        aucsTrain.append(metrics.auc(fpr, tpr))
        MAPTrain.append(metrics.average_precision_score(t, p))

    logging.warn(
        'Tweet - AUC@Train : %.4f , MAP : %.4f' % (sum(aucsTrain) / len(aucsTrain), sum(MAPTrain) / len(MAPTrain)))


def evaluateCold(model, thres):
    preds = model.predictWeibo(testUsers, samples)
    size = map(lambda x: len(d.weibo_action['train'][x]), testUsers)
    aucs = []
    MAP = []
    for i, (t, p) in enumerate(zip(truth, preds)):
        # p = map(lambda x: x + random.random() * 1e-15, p)
        ind = range(len(t))
        random.shuffle(ind)
        t = map(lambda x: t[x], ind)
        p = map(lambda x: p[x], ind)
        fpr, tpr, thresholds = metrics.roc_curve(t, np.array(p), pos_label=1)
        aucs.append(metrics.auc(fpr, tpr))
        MAP.append(metrics.average_precision_score(t, p))

    auc_groups = []
    for t in thres:
        sum = 0
        cnt = 0
        for s, a in zip(size, aucs):
            if s <= t:
                cnt += 1
                sum += a
        if cnt == 0:
            auc_groups.append(0)
        else:
            auc_groups.append(sum / cnt)
    logging.warn('Tweet - AUC@Test (Cold) : %s' % (', '.join(map(lambda x: '%.4f' % x, auc_groups))))

# model = TopicModel(d, K=100)
# print model.calcPerp()
# model.showTopics()
# model.save()
# evaluate(model)
# while True:
#     model.train(d, 10)
#     model.save()
#     evaluate()
