import numpy as np
import logging
from sklearn import metrics
import Data as d
import multiprocessing
import random
from functools import partial
import pickle


class ExactMatch:
    def __init__(self, d):
        pass

    def train(self, d):
        self.alignment = {}
        name2id = dict(map(lambda x: (hash(tuple(d.weibo_name[x])), x), d.weibo_users))
        for uid in d.douban_users:
            name = hash(tuple(d.douban_name[uid]))
            if name in name2id:
                self.alignment[uid] = name2id[name]
        print len(self.alignment)

    def align(self, d):
        return self.alignment


class TopAlign:
    def __init__(self, w):
        self.alignment = {}
        for uid in w:
            if len(w[uid]) > 0:
                list = w[uid].items()
                random.shuffle(list)
                list = sorted(list, key=lambda x: x[1])
                self.alignment[uid] = list[0][0]

    def train(self, d):
        pass

    def align(self, d):
        return self.alignment


def evaluate(aligner):
    alignment = aligner.align(d)
    hit = 0
    tot = 0
    for uid in filter(lambda x: x not in d.align_train, d.douban_users):
        if uid in alignment:
            tot += 1
            if alignment[uid] == d.alignment[uid]:
                hit += 1

    prec = hit / float(tot)
    recall = hit / float(len(d.alignment) - len(d.align_train))
    f1 = (2.0 * prec * recall) / (prec + recall)
    print 'Hit : %d, Precision : %.4f, Recall : %.4f, F1-Score : %.4f' % (hit,
                                                                          prec,
                                                                          recall, f1)


def evaluateAUC(aligner):
    pairs = d.alignment.items()
    t = [1] * len(pairs)
    for r in range(5):
        for i in range(len(pairs)):
            did=d.douban_users[random.randint(0,len(d.douban_users)-1)]
            wid=d.weibo_users[random.randint(0,len(d.weibo_users)-1)]
            pairs.append((did,wid))
            t.append(0)
    score = aligner.predictAlignScore(pairs)
    fpr, tpr, thresholds = metrics.roc_curve(np.array(t), np.array(score), pos_label=1)
    logging.info('Aligner - AUC@ALL : %.4f' % metrics.auc(fpr, tpr))


def distance(uid):
    a = d.douban_name[uid]
    res = {}
    for wid in d.weibo_users:
        b = d.weibo_name[wid]
        dist = [range(len(b) + 1) for x in range(len(a) + 1)]
        dist[len(a)][len(b)] = 100
        for i in range(len(a)):
            dist[i + 1][0] = i + 1
            curmin = 100
            for j in range(len(b)):
                dist[i + 1][j + 1] = min(dist[i + 1][j], dist[i][j + 1]) + 1
                if a[i] == b[j]:
                    dist[i + 1][j + 1] = min(dist[i + 1][j + 1], dist[i][j])
                curmin = min(curmin, dist[i + 1][j + 1])
            if (curmin > 5):
                break
        if dist[len(a)][len(b)] <= 5:
            res[wid] = dist[len(a)][len(b)]
    return res


def calcEditDistance():
    pool = multiprocessing.Pool()
    dist = pool.map(distance, d.douban_users)
    dist = dict(zip(d.douban_users, dist))
    pool.close()
    pickle.dump(dist, open('../models/editDistances', 'wb'))


def topAlign(sim):
    pairs = [(x, y, z) for x in sim for y, z in sim[x].items()]
    pairs.sort(key=lambda x: -x[2])
    matched = set()
    alignment = {}
    for x, y, s in pairs:
        if x in alignment or y in matched:
            continue
        alignment[x] = y
        matched.add(y)
    return alignment
