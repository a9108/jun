import random
import tensorflow as tf
from tensorflow.contrib import rnn
import logging
import os
import TweetRec
import NetAlign
import pickle


class JUMA:

    def setname(self, expname):
        self.expname = expname

    def __init__(self, d, douban_K=64, douban_alpha=1e-2, douban_gamma=1e3, evaluator=None, evaluationRound=0, weibo_K=64, word_K=64, weibo_alpha=1e-2, dump_iteration=0, isolate=False, aligner_alpha=1e-2,
                 expname='juma', constant=False):
        self.maxlen = 100
        self.weibo_K = weibo_K
        self.word_K = word_K
        self.weibo_alpha = weibo_alpha
        self.evaluator = evaluator
        self.evaluationRound = evaluationRound
        self.douban_K = douban_K
        self.douban_alpha = douban_alpha
        self.douban_gamma = douban_gamma
        self.aligner_alpha = aligner_alpha
        self.dump_iteration = dump_iteration
        if isolate:
            self.dump_iteration = 0
        self.isolate = isolate

        self.expname = expname

        config = tf.ConfigProto(allow_soft_placement=True,
                                log_device_placement=False)
        config.gpu_options.allow_growth = True
        self.session = tf.Session(config=config)
        self.M = {}
        param_settings = []

        self.douban_userId = dict(
            zip(d.douban_users, range(len(d.douban_users))))
        self.douban_itemList = list(d.douban_items)
        self.weibo_userId = dict(zip(d.weibo_users, range(len(d.weibo_users))))
        self.nWords = 10001

        # Douban Params
        param_settings.append(
            ('douban_U', [len(d.douban_users), self.douban_K], tf.random_normal))
        param_settings.append(
            ('douban_V', [len(d.douban_items), self.douban_K], tf.random_normal))
        param_settings.append(
            ('douban_userBias', [len(d.douban_users)], tf.zeros))
        param_settings.append(
            ('douban_itemBias', [len(d.douban_users)], tf.zeros))
        douban_layers_size = [self.douban_K * 3, 100, 50, 1]
        for i in range(len(douban_layers_size) - 1):
            param_settings.append(
                ('douban_W_%d' % i, [douban_layers_size[i], douban_layers_size[i + 1]], tf.random_normal))
            param_settings.append(('douban_bias_%d' %
                                   i, [douban_layers_size[i + 1]], tf.zeros))

        # Weibo Params
        param_settings.append(
            ('weibo_U', [len(self.weibo_userId), self.weibo_K], tf.random_normal))
        param_settings.append(
            ('weibo_word_embedding', [self.nWords, self.word_K], tf.random_normal))
        weibo_layers_size = [self.weibo_K + self.word_K, 100, 50, 1]
        for i in range(len(weibo_layers_size) - 1):
            param_settings.append(
                ('weibo_W_%d' % i, [weibo_layers_size[i], weibo_layers_size[i + 1]], tf.random_normal))
            param_settings.append(
                ('weibo_bias_%d' % i, [weibo_layers_size[i + 1]], tf.zeros))

        aligner_layer_size = [self.douban_K * 3, 100, 50, 1]
        for i in range(len(aligner_layer_size) - 1):
            param_settings.append(
                ('aligner_W_%d' % i, [aligner_layer_size[i], aligner_layer_size[i + 1]], tf.random_normal))
            param_settings.append(('aligner_bias_%d' %
                                   i, [aligner_layer_size[i + 1]], tf.zeros))

        if not isolate and os.path.exists('../models/%s.aligner' % self.expname):
            M = pickle.load(open('../models/%s.aligner' % self.expname, 'rb'))
            for param in M:
                self.M[param] = tf.Variable(M[param], name=param)
        if not isolate and os.path.exists('../models/%s.douban' % self.expname):
            M = pickle.load(open('../models/%s.douban' % self.expname, 'rb'))
            for param in M:
                if param == 'douban_U' and constant:
                    self.M[param] = tf.constant(M[param], name=param)
                else:
                    self.M[param] = tf.Variable(M[param], name=param)
        if not isolate and os.path.exists('../models/%s.weibo' % self.expname):
            M = pickle.load(open('../models/%s.weibo' % self.expname, 'rb'))
            for param in M:
                if param == 'weibo_U' and constant:
                    self.M[param] = tf.constant(M[param], name=param)
                else:
                    self.M[param] = tf.Variable(M[param], name=param)

        for name, shape, initializer in param_settings:
            if not name in self.M:
                self.M[name] = tf.Variable(initializer(shape), name=name)

        self.param_settings = param_settings
        # Douban Model
        self.douban_uid = tf.placeholder(tf.int32)
        self.douban_iid = tf.placeholder(tf.int32)
        self.douban_label = tf.placeholder(tf.float32)
        self.douban_eval_bias = tf.add(tf.gather(self.M['douban_userBias'], self.douban_uid),
                                       tf.gather(self.M['douban_itemBias'], self.douban_iid))
        self.douban_eval_U = tf.nn.relu(
            tf.gather(self.M['douban_U'], self.douban_uid))
        self.douban_eval_V = tf.gather(self.M['douban_V'], self.douban_iid)

        # self.douban_pred = tf.sigmoid(
        #     tf.add(self.douban_eval_bias,
        # tf.reduce_sum(tf.multiply(self.douban_eval_U, self.douban_eval_V),
        # 1)))

        lastLayer = tf.concat(
            [self.douban_eval_U, self.douban_eval_V, tf.multiply(self.douban_eval_U, self.douban_eval_V)], 1)
        douban_funcs = (
            [tf.nn.relu] * (len(douban_layers_size) - 2) + [tf.sigmoid])
        for i, func in enumerate(douban_funcs):
            lastLayer = func(tf.add(tf.matmul(lastLayer, self.M[
                             'douban_W_%d' % i]), self.M['douban_bias_%d' % i]))
        self.douban_pred = tf.reshape(lastLayer, [-1])

        self.douban_regulization = tf.reduce_mean(tf.square(self.M['douban_U'])) + tf.reduce_mean(
            tf.square(self.M['douban_V']))
        self.douban_loss = tf.reduce_sum(
            tf.square(tf.subtract(self.douban_pred, self.douban_label))) + self.douban_gamma * self.douban_regulization
        self.douban_learner = tf.train.AdamOptimizer(
            self.douban_alpha).minimize(self.douban_loss)

        # Weibo Model
        self.weibo_uid = tf.placeholder(tf.int32, shape=[None])
        self.weibo_wid = tf.placeholder(tf.int32, shape=[None, self.maxlen])
        self.weibo_label = tf.placeholder(tf.float32, shape=[None])
        # [None, maxlen, word_K]
        self.selected_embedding = tf.gather(
            self.M['weibo_word_embedding'], self.weibo_wid)
        # [None, word_K]
        lstm = rnn.BasicLSTMCell(word_K, forget_bias=1.0)
        outputs, states = rnn.static_rnn(lstm, tf.unstack(
            self.selected_embedding, self.maxlen, 1), dtype=tf.float32)
        self.sentence_embedding = outputs[-1]
        # self.sentence_embedding = tf.reduce_mean(self.selected_embedding, 1)
        # [None,weibo_K]
        self.weibo_user_embedding = tf.gather(
            self.M['weibo_U'], self.weibo_uid)
        # [None,weibo_K+word_K]
        lastLayer = tf.concat(
            [self.weibo_user_embedding, self.sentence_embedding], 1)

        weibo_funcs = (
            [tf.nn.relu] * (len(weibo_layers_size) - 2) + [tf.sigmoid])
        for i, func in enumerate(weibo_funcs):
            lastLayer = func(tf.add(tf.matmul(lastLayer, self.M[
                             'weibo_W_%d' % i]), self.M['weibo_bias_%d' % i]))

        self.weibo_output = tf.reshape(lastLayer, [-1])

        self.weibo_loss = tf.reduce_sum(
            tf.square(tf.subtract(self.weibo_output, self.weibo_label)))
        self.weibo_learner = tf.train.AdamOptimizer(
            self.weibo_alpha).minimize(self.weibo_loss)

        # Aligner Model
        self.aligner_did = tf.placeholder(tf.int32, shape=[None])
        self.aligner_wid = tf.placeholder(tf.int32, shape=[None])
        self.aligner_label = tf.placeholder(tf.float32, shape=[None])

        # [None,douban_K+weibo_K]
        douban_Embedding = tf.gather(self.M['douban_U'], self.aligner_did)
        weibo_Embedding = tf.gather(self.M['weibo_U'], self.aligner_wid)
        lastLayer = tf.concat([douban_Embedding, weibo_Embedding, tf.multiply(
            douban_Embedding, weibo_Embedding)], 1)
        aligner_funcs = (
            [tf.nn.relu] * (len(aligner_layer_size) - 2) + [tf.sigmoid])
        for i, func in enumerate(aligner_funcs):
            lastLayer = func(tf.add(tf.matmul(lastLayer, self.M[
                             'aligner_W_%d' % i]), self.M['aligner_bias_%d' % i]))

        self.aligner_pred = tf.reshape(lastLayer, [-1])
        self.aligner_loss = tf.reduce_sum(
            tf.square(tf.subtract(self.aligner_pred, self.aligner_label)))
        self.aligner_learner = tf.train.AdamOptimizer(
            self.aligner_alpha).minimize(self.aligner_loss)

        self.session.run(tf.global_variables_initializer())

        logging.info('JUMA Initialized')

    def dump(self, domains=['weibo', 'douban', 'aligner']):
        logging.info('Saving Model')
        for domain in domains:
            M = {}
            for param in filter(lambda x: x.startswith(domain), map(lambda x: x[0], self.param_settings)):
                M[param] = self.M[param].eval(self.session)
            pickle.dump(M, open('../models/%s.%s' %
                                (self.expname, domain), 'wb'))
        logging.info('Model Saved')

    def sliceWeibo(self, weibo):
        weibo = weibo[:min(len(weibo), self.maxlen)]
        weibo = weibo + [self.nWords - 1] * (self.maxlen - len(weibo))
        return weibo

    def getEvaluator(self, name):
        if self.evaluator == None:
            return None
        if not name in self.evaluator:
            return None
        return self.evaluator[name]

    def doubanDataFetcher(self, d):
        douban_train = []
        for uid in d.douban_users:
            for iid in d.douban_action['train'][uid]:
                douban_train.append((self.douban_userId[uid], iid, 1))
                douban_train.append((self.douban_userId[uid],
                                     self.douban_itemList[random.randint(0, len(self.douban_itemList)) - 1], 0))
        return douban_train

    def weiboDataFetcher(self, d):
        weibo_train = []
        for uid in d.weibo_users:
            if len(d.weibo_action['train'][uid]) > 0:
                for weibo in d.weibo_action['train'][uid]:
                    weibo_train.append(
                        (self.weibo_userId[uid], self.sliceWeibo(weibo.content), 1))
                for weibo in TweetRec.fetchNegWeibos(uid, len(d.weibo_action['train'][uid])):
                    weibo_train.append(
                        (self.weibo_userId[uid], self.sliceWeibo(weibo), 0))
        return weibo_train

    def alignerDataFetcher(self, d):
        aligner_train = []
        for did, wid in d.alignment.items():
            if random.random() < 0.4:
                continue
            did = self.douban_userId[did]
            wid = self.weibo_userId[wid]
            for i in range(10):
                aligner_train.append((did, wid, 1))
            for i in range(5):
                aligner_train.append(
                    (did, random.randint(0, len(self.weibo_userId) - 1), 0))
                aligner_train.append(
                    (random.randint(0, len(self.douban_userId) - 1), wid, 0))
        return aligner_train

    def train(self, d, round=100, updatingNets=['aligner', 'douban', 'weibo']):
        nets = {}
        nets['douban'] = {'dataFetcher': self.doubanDataFetcher,
                          'learner': self.douban_learner,
                          'loss': self.douban_loss,
                          'params': [self.douban_uid, self.douban_iid, self.douban_label],
                          'batchSize': 10000,
                          'evaluator': self.getEvaluator('movie')}
        nets['weibo'] = {'dataFetcher': self.weiboDataFetcher,
                         'learner': self.weibo_learner,
                         'loss': self.weibo_loss,
                         'params': [self.weibo_uid, self.weibo_wid, self.weibo_label],
                         'batchSize': 10000,
                         'evaluator': self.getEvaluator('tweet')}
        nets['aligner'] = {'dataFetcher': self.alignerDataFetcher,
                           'learner': self.aligner_learner,
                           'loss': self.aligner_loss,
                           'params': [self.aligner_did, self.aligner_wid, self.aligner_label],
                           'batchSize': 10000,
                           'evaluator': self.getEvaluator('aligner')}
        for net in updatingNets:
            logging.info('%s - Prepare Training Data' % net)
            nets[net]['data'] = nets[net]['dataFetcher'](d)
            logging.info('%s - Total Training Tuples : %d' %
                         (net, len(nets[net]['data'])))
        for r in range(round):
            for net in updatingNets:
                if r > 0 and self.evaluationRound > 0 and r % self.evaluationRound == 0 and nets[net][
                        'evaluator'] != None:
                    nets[net]['evaluator'](self)
                loss_value = 0
                random.shuffle(nets[net]['data'])
                batchSize = nets[net]['batchSize']
                for bid in range(len(nets[net]['data']) / batchSize):
                    dict = {}
                    for i, param in enumerate(nets[net]['params']):
                        dict[nets[net]['params'][i]] = map(lambda x: x[i],
                                                           nets[net]['data'][bid * batchSize:(bid + 1) * batchSize])
                    self.session.run(nets[net]['learner'], feed_dict=dict)
                    loss_value += self.session.run(nets[net]
                                                   ['loss'], feed_dict=dict)
                logging.info('%s - Round #%d : %.4f' % (net, r, loss_value))
            if self.dump_iteration > 0 and (r + 1) % self.dump_iteration == 0:
                self.dump(domains=updatingNets)
        logging.info('Training Finished')

    def predictDouban(self, uid, items):
        return self.session.run(self.douban_pred,
                                feed_dict={self.douban_uid: [self.douban_userId[uid]] * len(items),
                                           self.douban_iid: list(items)})

    def predictWeibo(self, users, samples):
        preds = []
        for i, user, sample in zip(range(len(users)), users, samples):
            uid = self.weibo_userId[user]
            sample = map(self.sliceWeibo, sample)
            preds.append(self.session.run(self.weibo_output,
                                          feed_dict={self.weibo_uid: [uid] * len(sample), self.weibo_wid: sample}))
        return preds

    def predictAlign(self, d):
        w = {}
        wids = map(lambda x: self.weibo_userId[x], d.weibo_users)
        for did in d.douban_users:
            dids = [self.douban_userId[did]] * len(wids)
            preds = self.session.run(self.aligner_pred, feed_dict={
                                     self.aligner_did: dids, self.aligner_wid: wids})
            w[did] = dict(zip(d.weibo_users, preds))
        return NetAlign.TopAlign(w).align(d)

    def predictAlignScore(self, pairs):
        did = map(lambda x: self.douban_userId[x[0]], pairs)
        wid = map(lambda x: self.weibo_userId[x[1]], pairs)
        pred = self.session.run(self.aligner_pred, feed_dict={
                                self.aligner_did: did, self.aligner_wid: wid})
        print pred[0:10]
        print pred[-10:]
        return pred
